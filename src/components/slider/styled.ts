import styled from "styled-components";
import {ReactComponent as Arrow} from './icons/arrow.svg';

export const WrapperSliderStyled = styled.div`
  overflow-x: hidden;
  width: 100%;
  position: relative;
  height: 590px;
  @media only screen and (max-width : 1024px) {
    height: 400px;
  }
  @media only screen and (max-width : 768px) {
    height: 280px;
  }
`

export const SliderStyled = styled.div`
  display: flex;
  flex-wrap: nowrap;
  position: absolute;
  left: -1130px;
`

export const SliderItemStyled = styled.div`
  width: 889px;
  height: 590px;
  margin-right: 20px;
  background-size: cover;
  background-position: center;
  @media only screen and (max-width : 1024px) {
    width: 591.07px;
    height: 400px;
    margin-right: 11px;
  }
  @media only screen and (max-width : 768px) {
    width: 419.9px;
    height: 280px;
  }
`

export const ArrowLeftStyled = styled(Arrow)`
  display: none;
  @media only screen and (max-width : 768px) {
    display: block;
    position: absolute;
    top: calc(50% - 10px);
    left: -23px;
    width: 26px;
    height: 20px;
  }
`

export const ArrowRightStyled = styled(Arrow)`
  display: none;
  @media only screen and (max-width : 768px) {
    display: block;
    position: absolute;
    top: calc(50% - 10px);
    right: 10px;
    width: 26px;
    height: 20px;
    transform: rotate(180deg);
    path{
      fill: white;
    }
  }
`