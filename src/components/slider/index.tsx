import {ArrowLeftStyled, ArrowRightStyled, SliderItemStyled, SliderStyled, WrapperSliderStyled} from "./styled";
import {FC, useCallback, useRef, useState} from "react";

export const Slider:FC<{images: Array<string>}> = (props) => {
  const [isTouch, setIsTouch] = useState(false)
  const [startX, setStartX] = useState(0)
  const [x, setX] = useState(0)
  const SliderRef:any = useRef()
  const WrapperSliderRef:any = useRef()

  const onMouseMove = useCallback((e:any) => {
    if(isTouch){
      setX(e.screenX)
    }
  },[isTouch])

  const onTouchMove = useCallback((e:any) => {
    if(isTouch){
      setX(e.nativeEvent.touches[0].screenX)
    }
  },[isTouch])

  const onTouchStop = useCallback(() => {
    setIsTouch(false)
    if(x - startX > 0){
      setStartX(x)
    }
    if(SliderRef.current.offsetWidth - WrapperSliderRef.current.offsetWidth < - (x-startX)){
      setStartX(SliderRef.current.offsetWidth - WrapperSliderRef.current.offsetWidth + x)
    }
  },[startX, x])

  const onClickLeftArrow = () => {
    const newX = x + WrapperSliderRef.current.offsetWidth
    if(newX - startX > 0){
      setX(startX)
    }else{
      setX(newX)
    }
  }

  const onClickRightArrow = () => {
    const newX = x - WrapperSliderRef.current.offsetWidth
    if(SliderRef.current.offsetWidth - WrapperSliderRef.current.offsetWidth < - (newX-startX)){
      setX(- (SliderRef.current.offsetWidth - WrapperSliderRef.current.offsetWidth) + startX)
    }else{
      setX(newX)
    }
  }

  return (
    <div style={{position: 'relative'}}>
      <ArrowLeftStyled onClick={onClickLeftArrow}/>
      <WrapperSliderStyled
        onMouseMove={onMouseMove}
        onMouseDown={(e)=>{
          setIsTouch(true);
          setStartX(e.screenX - x + startX);
          setX(e.screenX)}
        }
        onMouseUp={()=>onTouchStop()}
        onMouseLeave={()=>onTouchStop()}
        onTouchMove={onTouchMove}
        onTouchStart={(e)=>{
          setIsTouch(true);
          setStartX(e.nativeEvent.touches[0].screenX - x + startX);
          setX(e.nativeEvent.touches[0].screenX)}
        }
        onTouchEnd={()=>{onTouchStop()}}
        ref={WrapperSliderRef}
      >
        <SliderStyled style={{left: `${x-startX}px`,transitionDuration: isTouch ? 'unset' : '0.3s'}} ref={SliderRef}>
          {props.images.map((image)=><SliderItemStyled style={{backgroundImage: `url(${image})`}}/>)}
        </SliderStyled>
      </WrapperSliderStyled>
      <ArrowRightStyled onClick={onClickRightArrow}/>
    </div>
  )
}