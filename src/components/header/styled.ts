import styled from "styled-components";

export const HeaderStyled = styled.div<{isView: boolean}>`
  height: 53px;
  width: 100%;
  background: #FFF9EE;
  display: flex;
  position: fixed;
  z-index: 2;
  transition-duration: 0.14s;
  @media only screen and (max-width : 768px) {
    top: 0 !important;
  }
  ${props => props.isView ? 'top: 0;' : 'top: -53px;'}
`

export const ItemListStyled = styled.div<{isView: boolean}>`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-left: 13px;
  height: 100%;
  width: 59%;
  @media only screen and (max-width : 1024px) {
    width: 90%;
  }
  @media only screen and (max-width : 768px) {
    padding-left: 5%;
    margin-left: 0;
  }
  @media only screen and (max-width : 576px) {
    width: 221px;
    height: 220px;
    position: absolute;
    top: 0;
    background: #FFF9EE;
    flex-direction: column;
    padding-top: 88px;
    padding-bottom: 80px;
    align-items: normal;
    border-right: 1px solid rgba(0,0,0,0.3);
    border-bottom: 1px solid rgba(0,0,0,0.3);
    transition-duration: 0.3s;
    ${props => props.isView ? 'left: -100%;' : 'left: 0;'}
  }
`

export const ItemStyled = styled.div`
  //styleName: Мелкий текст;
  font-family: 'LatoWebSemibold', sans-serif;
  font-weight: 500;
  font-style: normal;
  font-size: 14px;
  letter-spacing: -0.165px;
  margin-top: 6px;
`

export const HeaderLeftStyled = styled.div`
  flex: 1 1 707px;
  @media only screen and (max-width : 1024px) {
    flex: 1 1 345px;
  }
  @media only screen and (max-width : 768px) {
    display: none;
  }
`

export const HeaderRightStyled = styled.div`
  flex: 0 1 1212px;
  @media only screen and (max-width : 1024px) {
    flex: 1 1 679px;
  }
`
export const WrapperMobileButtonStyled = styled.div`
  @media only screen and (max-width : 576px) {
    position: absolute;
    left: 20px;
    top: 10px;
    cursor: pointer;
    width: 30px;
    height: 30px;
    z-index: 2;
  }
`

export const MobileButtonStyled = styled.div<{isView: boolean}>`
  @media only screen and (max-width : 576px) {
    position: absolute;
    width: 30px;
    height: 3px;
    background: black;
    left: 0px;
    top: 13px;
    cursor: pointer;
    border-radius: 4px;
    ${props => props.isView ? 'background: black;' : 'background: rgba(0,0,0,0);'}
    transition-duration: 0.2s;
    &:before{
      content: '';
      display: block;
      width: 30px;
      height: 3px;
      background: black;
      position: absolute;
      left: 0;
      top: -10px;
      border-radius: 4px;
      ${props => props.isView ? '' : 'transform: rotate(45deg); top: 0px;'}
      transition-duration: 0.2s;
    }
    &:after{
      content: '';
      display: block;
      width: 30px;
      height: 3px;
      background: black;
      position: absolute;
      left: 0;
      top: 10px;
      border-radius: 4px;
      ${props => props.isView ? '' : 'transform: rotate(-45deg); top: 0px;'}
      transition-duration: 0.2s;
    }
  }
`