import {
  HeaderLeftStyled,
  HeaderRightStyled,
  HeaderStyled,
  ItemStyled,
  ItemListStyled, MobileButtonStyled, WrapperMobileButtonStyled
} from "./styled";
import {useEffect, useState} from "react";
import {Screen} from "../../store/screen/model";

let prevValue = 0

export const Header = () => {
  const [isView, setIsView] = useState(true)
  const screenExtension = Screen.useExtension()

  useEffect(()=>{
    if(screenExtension !== 'ipad' && screenExtension !== 'mobile'){
      document.getElementsByTagName('body')[0].onscroll = (e:any) => {
        const value = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0
        if(prevValue > value){
          setIsView(true)
        }else{
          if(value > 53) {
            setIsView(false)
          }
        }
        prevValue = value
      };
    }else{
      document.getElementsByTagName('body')[0].onscroll = () => {}
    }
  },[screenExtension])

  return (
    <HeaderStyled isView={isView}>
      <HeaderLeftStyled>

      </HeaderLeftStyled>
      <HeaderRightStyled>
        <WrapperMobileButtonStyled onClick={()=>setIsView(!isView)}>
          <MobileButtonStyled isView={isView}/>
        </WrapperMobileButtonStyled>
        <ItemListStyled isView={isView}>
          <ItemStyled>Коттеджи</ItemStyled>
          <ItemStyled>Локации</ItemStyled>
          <ItemStyled>О проекте</ItemStyled>
          <ItemStyled>Бронирование</ItemStyled>
        </ItemListStyled>
      </HeaderRightStyled>
    </HeaderStyled>
  )
}