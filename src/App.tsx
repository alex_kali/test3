import React, {memo} from 'react';
import {
  BrowserRouter as Router,
  useRoutes
} from "react-router-dom";
import './App.css';
import {HouseList} from "./pages/houseList";
import {Header} from "./components/header";

function App() {
  const AppRoutes = memo(() => {
    return useRoutes([
      { path: "/", element: <HouseList /> },
    ]);
  });
  return (
    <Router>
      <Header/>
      <AppRoutes />
    </Router>
  );
}

export default App;
