import {FC} from "react";
import {Menu} from "./menu";
import {HouseListStyled, HouseStyled, WrapperHouseListStyled} from "./styled";
import {HouseView} from "./house";
import {House} from "../../store/houseList/model";

export const HouseList:FC = () => {
  const houseList = House.useData()

  if(!houseList){
    return null
  }

  return (
    <WrapperHouseListStyled>
      <Menu items={houseList.map((house)=> house.name)}/>
      <HouseListStyled>
        {houseList.map((house)=><HouseView house={house} key={house.id}/>)}
      </HouseListStyled>
    </WrapperHouseListStyled>
  )
}