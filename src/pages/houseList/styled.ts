import styled from "styled-components";

export const MenuStyled = styled.div`
  display: block;
  top: 35vh;
  flex: 1 1 707px;
  position: sticky;
  height: 70vh;
  @media only screen and (max-width : 1024px) {
    top: 32vh;
    flex: 1 1 345px;
  }
  @media only screen and (max-width : 768px) {
    display: none;
  }
`

export const MenuTitleStyled = styled.div`
  margin-bottom: 23px;
  font-style: normal;
  font-weight: 700;
  font-size: 24px;
  line-height: 140%;
  font-family: 'LatoWebBold';
  @media only screen and (max-width : 1024px) {
    font-size: 20px;
  }
`

export const MenuItemStyled = styled.div<{isVisible: boolean}>`
  margin-bottom: 16px;
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  line-height: 150%;
  font-family: 'LatoWeb', sans-serif;
  @media only screen and (max-width : 1024px) {
    font-size: 16px;
  }
  ${props => props.isVisible ? 'opacity: 1;' : 'opacity: 0.5;'}
`

export const MenuItemListStyled = styled.div`
  height: 100%;
  width: 250px;
  margin-left: calc(30% - 50px);
  margin-top: 2px;
  @media only screen and (max-width : 1024px) {
    margin-top: -5px;
    margin-left: calc(30% - 15px);
  }
`


export const HouseStyled = styled.div`
  margin-top: 106px;
  margin-bottom: 20px;
  width: 100%;
  &:first-child{
    margin-top: 160px;
  }
  @media only screen and (max-width : 768px) {
    margin-top: 90px;
    &:first-child{
      margin-top: 90px;
    }
  }
`

export const WrapperHouseListStyled = styled.div`
  display: flex;
`

export const HouseListStyled = styled.div`
  flex: 1 1 1212px;
  overflow-x: hidden;
  @media only screen and (max-width : 1024px) {
    flex: 1 1 679px;
  }
  @media only screen and (max-width : 768px) {
    flex: 1 1 90%;
    padding-left: 5%;
  }
`

export const HouseTitleStyled = styled.div`
  font-family: 'Orbitron';
  font-style: normal;
  font-weight: 400;
  font-size: 64px;
  line-height: 80px;
  letter-spacing: 2px;
  margin-bottom: 5px;
  @media only screen and (max-width : 940px) {
    font-size: 45px;
    letter-spacing: 1px;
  }
  @media only screen and (max-width : 768px) {
    font-size: 36px;
    line-height: 44px;
    letter-spacing: 2px;
  }
`

export const HouseTextStyled = styled.div`
  width: 75%;
  margin-top: 27px;
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  line-height: 150%;
  font-family: 'LatoWeb', sans-serif;
  @media only screen and (max-width : 1024px) {
    font-size: 16px;
    width: 97%;
  }
`

export const HousePriceStyled = styled.div`
  font-style: normal;
  font-weight: 700;
  font-size: 24px;
  line-height: 140%;
  font-family: 'LatoWebBold';
  margin-top: 24px;
  @media only screen and (max-width : 1024px) {
    font-size: 20px;
  }
`