import {FC} from "react";
import {HousePriceStyled, HouseStyled, HouseTextStyled, HouseTitleStyled} from "./styled";
import {Slider} from "../../components/slider";
import {IHouse} from "../../store/houseList/model";


export const HouseView:FC<{house: IHouse}> = (props) => {
  return (
    <HouseStyled>
      <HouseTitleStyled id={props.house.name}>{props.house.name}</HouseTitleStyled>
      <Slider images={props.house.images}/>
      <HouseTextStyled>{props.house.description}</HouseTextStyled>
      <HousePriceStyled>от {props.house.price} руб./сут</HousePriceStyled>
    </HouseStyled>
  )
}