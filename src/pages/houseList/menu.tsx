import {FC, useEffect, useState} from "react";
import {MenuItemListStyled, MenuItemStyled, MenuStyled, MenuTitleStyled} from "./styled";
import {elementInViewport} from "../../utils/elementInViewport";

export const Menu:FC<{items: Array<string>}> = (props) => {
  const [activeTitle, setActiveTitle] = useState(props.items[0])

  useEffect(()=>{
    window.addEventListener('scroll',() => {
      for(let i of props.items){
        const blockPosition = (document.getElementById(i) as any)
        if(elementInViewport(blockPosition)){
          setActiveTitle(i)
          break
        }
      }
    });
  },[])

  return (
    <MenuStyled>
      <MenuItemListStyled>
        <MenuTitleStyled>Коттеджи A-frame</MenuTitleStyled>
        {props.items.map((item)=><MenuItemStyled isVisible={item === activeTitle}>{item}</MenuItemStyled>)}
      </MenuItemListStyled>
    </MenuStyled>
  )
}