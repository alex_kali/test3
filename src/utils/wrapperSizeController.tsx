import React, {FC, memo, useEffect} from "react";
import {useWindowSize} from "react-use-size";
import {useDispatch, useSelector} from "react-redux";
import {
  screenExtensionSelector,
  screenOrientationSelector,
  setScreenExtension,
  setScreenOrientation
} from "../store/screen/slice";

export const OptimizedSizeController: FC = memo((props) => {
  const { height, width } = useWindowSize();
  const screenExtension = useSelector(screenExtensionSelector)
  const screenOrientation = useSelector(screenOrientationSelector)

  const dispatch = useDispatch()
  useEffect(()=>{
    if(width >= 1600){
      // largePc
      if(screenExtension !== "largePc"){
        dispatch(setScreenExtension('largePc'))
        if(screenOrientation !== undefined){
          dispatch(setScreenOrientation(undefined))
        }
      }
    }else if(width >= 1200){
      // pc
      if(screenExtension !== "pc"){
        dispatch(setScreenExtension('pc'))
        if(screenOrientation !== undefined){
          dispatch(setScreenOrientation(undefined))
        }
      }
    }else if(width >= 992){
      //smallPc
      if(screenExtension !== "smallPc"){
        dispatch(setScreenExtension('smallPc'))
        if(screenOrientation !== undefined){
          dispatch(setScreenOrientation(undefined))
        }
      }
    }else if(width >= 576){
      //ipad
      if(screenExtension !== "ipad"){
        dispatch(setScreenExtension('ipad'))
      }
      if(width > height){
        if(screenOrientation !== 'ipadLandscape'){
          dispatch(setScreenOrientation('ipadLandscape'))
        }
      }else{
        if(screenOrientation !== 'ipadPortrait'){
          dispatch(setScreenOrientation('ipadPortrait'))
        }
      }
    }else{
      //mobile
      if(screenExtension !== "mobile"){
        dispatch(setScreenExtension('mobile'))
      }
      if(width > height){
        if(screenOrientation !== 'mobileLandscape'){
          dispatch(setScreenOrientation('mobileLandscape'))
        }
      }else{
        if(screenOrientation !== 'mobilePortrait'){
          dispatch(setScreenOrientation('mobilePortrait'))
        }
      }
    }
  },[width, height])

  return (
    <></>
  );
});

