import {RootState} from "../store";
import {useSelector} from "react-redux";

export const House = {
  useData: () => useSelector((state: RootState) => state.houseList.houseList) as Array<IHouse> | undefined
}





export interface IHouse{
  id: number | string;
  name: string;
  description: string;
  images: Array<string>;
  price: number;
}