import {RootState} from "../store";
import {useSelector} from "react-redux";
import {screenOrientationSelector} from "./slice";

export const Screen = {
  useExtension: () => useSelector((state: RootState) => state.screenController.screenExtension),
  useOrientation: () => useSelector((state: RootState) => state.screenController.screenOrientation),
}