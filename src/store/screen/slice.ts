import {createSlice, current} from "@reduxjs/toolkit";
import {RootState} from "../store";

//screens largePc , pc , smallPc ,ipad, mobile
//orientation ipadPortrait , ipadLandscape , mobilePortrait , mobileLandscape , none

export const initialState: any = {
  screenExtension: '',
  screenOrientation: '',
};


export const screenControllerSlice = createSlice({
  name: 'screenController',
  initialState,
  reducers: {
    setScreenExtension: (state, action) => {
      state.screenExtension = action.payload;
    },
    setScreenOrientation: (state, action) => {
      state.screenOrientation = action.payload;
    },
  },
});

export const { setScreenExtension, setScreenOrientation } = screenControllerSlice.actions;


export const screenExtensionSelector = (state: RootState) => state.screenController.screenExtension;

export const screenOrientationSelector = (state: RootState) => state.screenController.screenOrientation;


export default screenControllerSlice.reducer;