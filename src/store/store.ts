import {configureStore, ThunkAction, Action, combineReducers} from '@reduxjs/toolkit';
import houseListReducer from './houseList/slices';
import screenControllerReducer from "./screen/slice";

const combinedReducer = combineReducers({
  houseList: houseListReducer,
  screenController: screenControllerReducer,
});

const rootReducer = (state:any, action:any) => {
  return combinedReducer(state, action);
};

export const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export type RootState = ReturnType<typeof store.getState>;

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
  >;